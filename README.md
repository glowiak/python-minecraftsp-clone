# python-minecraftsp-clone

Python clone of the old alpha launcher for Minecraft

It looks like the old alpha launcher, (after 2013 broken, read: anjocaido)

Identically to it, launches 1.5.2 by default,

it's very minimalist - the only controls/fields are the play button, the force update checkbox and the login field.

It does not open MC in the same window, because it's not written in java

I am too dumb to know how to implement M$ auth, so if you haven't purchased mc pls do it.

### Running

Due to some internal stuff, it can run only on UNIX-like OSes, if you use winblow$, it may (but don't have to) work on WSL.

The dependiences are: python3, PyQt5, unzip, java

Run it with:

    curl -L https://codeberg.org/glowiak/python-minecraftsp-clone/raw/branch/master/MinecraftSP.py | python3

Or if ya like the hac*ing in the ol' good times, download from http://chomikuj.pl/glowiak1111/MinecraftSP-python/MinecraftSP,8131143699.py (chomikuj.pl is the most hacker-looking website i know) and run.

### Arguments

You can change os recorganized by launcher (in case you don't use Linux) with the first agument (only small letters!)

You can change the amount of memory with the second argument (f.e 256M)

You can change the java executable with the third argument

### Credits

Minecraft is made by Mojang Specifications

both the dirt texture and the minecraft alpha logo are just random images from duckduckgo

assets are from minecraft.net

The Launcher itself does not ship any images/assets.

## Note

I am not associated with Mojang/Microsoft.