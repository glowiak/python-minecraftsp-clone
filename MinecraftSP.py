#!/usr/bin/env python3
import os, sys, random, requests, os.path
try:
    from PyQt5.QtCore import *
    from PyQt5.QtGui import *
    from PyQt5.QtWidgets import *
except:
    os.system("pip install PyQt5")
    from PyQt5.QtCore import *
    from PyQt5.QtGui import *
    from PyQt5.QtWidgets import *
from urllib.parse import urlparse
    
gameJars = [ "https://libraries.minecraft.net/net/minecraft/launchwrapper/1.5/launchwrapper-1.5.jar", "https://libraries.minecraft.net/net/sf/jopt-simple/jopt-simple/4.5/jopt-simple-4.5.jar", "https://libraries.minecraft.net/org/ow2/asm/asm-all/4.1/asm-all-4.1.jar https://libraries.minecraft.net/net/java/jinput/jinput/2.0.5/jinput-2.0.5.jar", "https://libraries.minecraft.net/net/java/jutils/jutils/1.0.0/jutils-1.0.0.jar", "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl/2.9.0/lwjgl-2.9.0.jar", "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl_util/2.9.0/lwjgl_util-2.9.0.jar", "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.0/lwjgl-platform-2.9.0-natives-linux.jar", "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.0/lwjgl-platform-2.9.0-natives-osx.jar", "https://libraries.minecraft.net/net/java/jinput/jinput-platform/2.0.5/jinput-platform-2.0.5-natives-linux.jar", "https://libraries.minecraft.net/net/java/jinput/jinput-platform/2.0.5/jinput-platform-2.0.5-natives-osx.jar", "https://libraries.minecraft.net/com/mojang/netty/1.8.8/netty-1.8.8.jar", "https://libraries.minecraft.net/java3d/vecmath/1.3.1/vecmath-1.3.1.jar", "https://libraries.minecraft.net/net/sf/trove4j/trove4j/3.0.3/trove4j-3.0.3.jar", "https://libraries.minecraft.net/com/ibm/icu/icu4j-core-mojang/51.2/icu4j-core-mojang-51.2.jar", "https://libraries.minecraft.net/com/paulscode/codecjorbis/20101023/codecjorbis-20101023.jar", "https://libraries.minecraft.net/com/paulscode/codecwav/20101023/codecwav-20101023.jar", "https://libraries.minecraft.net/com/paulscode/libraryjavasound/20101123/libraryjavasound-20101123.jar", "https://libraries.minecraft.net/com/paulscode/librarylwjglopenal/20100824/librarylwjglopenal-20100824.jar", "https://libraries.minecraft.net/com/paulscode/soundsystem/20120107/soundsystem-20120107.jar", "https://libraries.minecraft.net/io/netty/netty-all/4.0.10.Final/netty-all-4.0.10.Final.jar", "https://libraries.minecraft.net/com/google/guava/guava/15.0/guava-15.0.jar", "https://libraries.minecraft.net/org/apache/commons/commons-lang3/3.1/commons-lang3-3.1.jar", "https://libraries.minecraft.net/commons-io/commons-io/2.4/commons-io-2.4.jar", "https://libraries.minecraft.net/commons-codec/commons-codec/1.9/commons-codec-1.9.jar", "https://libraries.minecraft.net/com/google/code/gson/gson/2.2.4/gson-2.2.4.jar", "https://libraries.minecraft.net/com/mojang/authlib/1.5.12/authlib-1.5.12.jar", "https://libraries.minecraft.net/org/apache/logging/log4j/log4j-api/2.0-beta9/log4j-api-2.0-beta9.jar", "https://libraries.minecraft.net/org/apache/logging/log4j/log4j-core/2.0-beta9/log4j-core-2.0-beta9.jar", "https://libraries.minecraft.net/argo/argo/2.25_fixed/argo-2.25_fixed.jar", "https://libraries.minecraft.net/org/bouncycastle/bcprov-jdk15on/1.47/bcprov-jdk15on-1.47.jar", "https://libraries.minecraft.net/oshi-project/oshi-core/1.1/oshi-core-1.1.jar", "https://libraries.minecraft.net/net/java/dev/jna/jna/3.4.0/jna-3.4.0.jar", "https://libraries.minecraft.net/net/java/dev/jna/platform/3.4.0/platform-3.4.0.jar", "https://libraries.minecraft.net/com/mojang/realms/1.8.19/realms-1.8.19.jar", "https://libraries.minecraft.net/org/apache/commons/commons-compress/1.8.1/commons-compress-1.8.1.jar", "https://libraries.minecraft.net/org/apache/httpcomponents/httpclient/4.3.3/httpclient-4.3.3.jar", "https://libraries.minecraft.net/commons-logging/commons-logging/1.1.3/commons-logging-1.1.3.jar", "https://libraries.minecraft.net/org/apache/httpcomponents/httpcore/4.3.2/httpcore-4.3.2.jar", "https://libraries.minecraft.net/it/unimi/dsi/fastutil/7.0.12_mojang/fastutil-7.0.12_mojang.jar" ]

# enable this one for the beta experience
# mainGameJar = "https://archive.org/download/Minecraft-JE-Beta/Prereleases/b1.6-tb3/b1.6-tb3.jar"

# the normal jar
mainGameJar = "https://launcher.mojang.com/v1/objects/465378c9dc2f779ae1d6e8046ebc46fb53a57968/client.jar"

gameDir = f"{os.environ['HOME']}/.minecraft"
print(f"Game dir: {gameDir}")

mainClass = "net.minecraft.client.Minecraft"

proxyServer = "betacraft.uk"

try:
    usedOS = sys.argv[1]
except:
    usedOS = "linux"

try:
    gameMem = sys.argv[2]
except:
    gameMem = "256M"

try:
    javaPath = sys.argv[3]
except:
    javaPath = "java" # the one on your pc
    # javaPath = "/usr/home/glowiak/Downloads/jdk1.6.0_07/bin/java" # the one on my pc

def main():
    app = QApplication(sys.argv)
    w = QWidget()
    w.setWindowTitle("Minecraft Launcher")
    w.setGeometry(100,100,800,600)
    
    # create the dirs if not exist
    if not os.path.exists(gameDir):
        print(f"Create {gameDir}")
        os.mkdir(gameDir)
    if not os.path.exists(f"{gameDir}/bin"):
        print(f"Create {gameDir}/bin")
        os.mkdir(f"{gameDir}/bin")
    if not os.path.exists(f"{gameDir}/bin/natives"):
        print(f"Create {gameDir}/bin/natives")
        os.mkdir(f"{gameDir}/bin/natives")
    if not os.path.exists(f"{gameDir}/libs"):
        print(f"Create {gameDir}/libs")
        os.mkdir(f"{gameDir}/libs")
    
    # download the icon if not exists
    if not os.path.exists(f"{gameDir}/dirt.png"):
        print(f"Downloading https://www.fractalcamo.com/uploads/5/9/0/2/5902948/s189772745713394276_p3856_i147_w750.jpeg")
        rd = requests.get("https://www.fractalcamo.com/uploads/5/9/0/2/5902948/s189772745713394276_p3856_i147_w750.jpeg", allow_redirects = True)
        open(f"{gameDir}/dirt.png", "wb").write(rd.content)
    if not os.path.exists(f"{gameDir}/minecraft.png"):
        print(f"Downloading http://s3.amazonaws.com/s3.timetoast.com/public/uploads/photos/2103813/MinecraftLogo.gif")
        rm = requests.get("http://s3.amazonaws.com/s3.timetoast.com/public/uploads/photos/2103813/MinecraftLogo.gif", allow_redirects = True)
        open(f"{gameDir}/minecraft.png", "wb").write(rm.content)
    
    background = QLabel(w)
    background.resize(1850,650)
    background.move(0,0)
    background.setPixmap(QPixmap(f"{gameDir}/dirt.png").scaledToWidth(2850).scaledToHeight(1650))
    
    mcLogo = QLabel(w)
    mcLogo.setPixmap(QPixmap(f"{gameDir}/minecraft.png").scaledToWidth(300).scaledToHeight(150))
    mcLogo.move(10,50)
    
    loginText = QLabel(w)
    loginText.setText("Login:")
    loginText.move(250,300)
    
    global login
    global forceUpd
    
    login = QLineEdit(w)
    login.resize(150,20)
    login.move(295,300)
    if os.path.exists(f"{gameDir}/nick.txt"):
        rn = open(f"{gameDir}/nick.txt", "r")
        login.setText(rn.read())
        rn.close()
    
    playButt = QPushButton(w)
    playButt.resize(100,20)
    playButt.setText("Play")
    playButt.move(445,300)
    playButt.clicked.connect(runGame)
    
    forceUpd = QCheckBox(w)
    forceUpd.setText("Force Update")
    forceUpd.move(295,320)
    
    w.show()
    
    sys.exit(app.exec_())

def runGame():
    # save the nick
    if os.path.exists(f"{gameDir}/nick.txt"):
        os.remove(f"{gameDir}/nick.txt")
    sn = open(f"{gameDir}/nick.txt", "w")
    sn.write(login.text())
    sn.close()
    
    # download the libraries
    for i in range(len(gameJars)):
        if not os.path.exists(f"{gameDir}/libs/{os.path.basename(urlparse(gameJars[i]).path)}"):
            print(f"Downloading {gameJars[i]}")
            r = requests.get(gameJars[i], allow_redirects = True)
            open(f"{gameDir}/libs/{os.path.basename(urlparse(gameJars[i]).path)}", "wb").write(r.content)
        else:
            print("is")

    # extract natives
    os.system(f"echo A | unzip {gameDir}/libs/lwjgl-platform-2.9.0-natives-{usedOS}.jar -d {gameDir}/bin/natives/")
    os.system(f"echo A | unzip {gameDir}/libs/jinput-platform-2.0.5-natives-{usedOS}.jar -d {gameDir}/bin/natives/")
    
    # download the version jar if empty or if force update is enabled
    if not os.path.exists(f"{gameDir}/bin/minecraft.jar") or forceUpd.isChecked() == True:
        if forceUpd.isChecked() == True:
            print(f"Delete {gameDir}/bin/minecraft.jar")
            os.remove(f"{gameDir}/bin/minecraft.jar")
        print(f"Downloading {mainGameJar}")
        rv = requests.get(mainGameJar, allow_redirects = True)
        open(f"{gameDir}/bin/minecraft.jar", "wb").write(rv.content)
    
    # run the game
    os.system(f"{javaPath} -Xmx{gameMem} -Xmn{gameMem} -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy -Djava.library.path={gameDir}/bin/natives/ -cp {build_classpath()}{gameDir}/bin/minecraft.jar -Dhttp.proxyHost={proxyServer} -Djava.util.Arrays.useLegacyMergeSort=true {mainClass} {login.text()} test123 --gameDir {gameDir}")

def build_classpath():
    # build the classpath
    cp = ""
    for i in os.listdir(f"{gameDir}/libs/"):
        cp = f"{gameDir}/libs/{i}:{cp}"
    return cp

if __name__ == '__main__':
    main()
